function ValidateAllData(cells,i)
{
    
    var username = cells[0], status = cells[1], qualification = cells[2], language = cells[3],
        experience = cells[4], firstName = cells[5], middleName = cells[6], lastName = cells[7], contact = cells[8],
        email = cells[9], gender = cells[10], imageId = cells[11], address = cells[12], city = cells[13], 
        locality = cells[14], district = cells[15], state = cells[16], country = cells[17], pincode = cells[18];
        
    ValidateUserName(username,i)
    ValidateStatus(status,i);
    ValidateQualification(qualification,i);
    ValidateLanguage(language,i);
    ValidateExperience(experience,i);
    ValidateFirstName(firstName,i);
    ValidateMiddleName(middleName,i);
    ValidateLastName(lastName,i);
    ValidateContact(contact,i);
    ValidateEmail(email,i);
    ValidateGender(gender,i);
    ValidateImageId(imageId,i);
    ValidateAddress(address,i);
    ValidateCity(city,i);
    ValidateLocality(locality,i);
    ValidateDistrict(district,i);
    ValidateState(state,i);
    ValidateCountry(country,i);
    ValidatePinCode(pincode,i)
}



        

