function ValidateState(state,i)
{
    var letters = /^[A-Za-z]+$/;
    if(!(state.match(letters)))
    {
        if(state == "")
        {
            var textdata = document.createTextNode("State is Mandatory in " + i + " Row.")
            PrintData(textdata);
        }
        else
        {
            var textdata = document.createTextNode("State should contain only Alphabets in " + i + " Row.")
            PrintData(textdata);
        }
    }
}