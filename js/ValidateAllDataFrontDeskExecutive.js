function ValidateAllDataFrontDeskExecutive(cells,i)
{
    
    var serviceCentreId = cells[0], username = cells[1], status = cells[2], qualification = cells[3], language = cells[4],
        experience = cells[5], firstName = cells[6], middleName = cells[7], lastName = cells[8], contact = cells[9],
        email = cells[10], gender = cells[11], imageId = cells[12], address = cells[13], city = cells[14], 
        locality = cells[15], district = cells[16], state = cells[17], country = cells[18], pincode = cells[19];
        
    ValidateserviceCentreId(serviceCentreId,i);
    ValidateUserName(username,i)
    ValidateStatus(status,i);
    ValidateQualification(qualification,i);
    ValidateLanguage(language,i);
    ValidateExperience(experience,i);
    ValidateFirstName(firstName,i);
    ValidateMiddleName(middleName,i);
    ValidateLastName(lastName,i);
    ValidateContact(contact,i);
    ValidateEmail(email,i);
    ValidateGender(gender,i);
    ValidateImageId(imageId,i);
    ValidateAddress(address,i);
    ValidateCity(city,i);
    ValidateLocality(locality,i);
    ValidateDistrict(district,i);
    ValidateState(state,i);
    ValidateCountry(country,i);
    ValidatePinCode(pincode,i)
}



        

