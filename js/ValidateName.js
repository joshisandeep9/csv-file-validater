function ValidateName(Name,i)
{
    var letters = /^[A-Za-z ]+$/;
    if(!(Name.match(letters)))
    {
        if(Name == "")
        {
            var textdata = document.createTextNode("Name is Missing in " + i + " Row.")
            PrintData(textdata);
        }
        else
        {
            var textdata = document.createTextNode("Name is invalid in " + i + " Row.")
            PrintData(textdata);
        }
    }
}