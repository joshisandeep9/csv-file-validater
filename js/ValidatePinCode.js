function ValidatePinCode(pincode,i)
{
    var regexp = /^\d{6}$/;
    if(!(pincode.match(regexp)))
    {
        if(pincode == "")
        {
            var textdata = document.createTextNode("Pin Code is Mandatory in " + i + " Row.")
            PrintData(textdata);
        }
        else
        {
            var textdata = document.createTextNode("Pin code should contain only 6 digits Numeric Value in " + i + " Row.")
            PrintData(textdata);
        }
    }
    
}
