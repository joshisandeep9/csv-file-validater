function ValidateAllDataCallCenterAdmin(cells,i)
{
    
    var address = cells[0], contact = cells[1], country = cells[2], state = cells[3], email = cells[4], 
        firstName = cells[5], lastName = cells[6], middleName = cells[7], username = cells[8], 
        language = cells[9], experience = cells[10], qualification = cells[11], gender = cells[12],
        status = cells[13], imageId = cells[14], dob = cells[15];
    
    ValidateAddress(address,i);
    ValidateContact(contact,i);
    ValidateCountry(country,i);
    ValidateState(state,i);
    ValidateEmail(email,i);
    ValidateFirstName(firstName,i);
    ValidateLastName(lastName,i);
    ValidateMiddleName(middleName,i);
    ValidateUserName(username,i)
    ValidateLanguage(language,i);
    ValidateExperience(experience,i);
    ValidateQualification(qualification,i);
    ValidateGender(gender,i);
    ValidateStatus(status,i);
    ValidateImageId(imageId,i);
    ValidateDOB(dob,i);
}



        

