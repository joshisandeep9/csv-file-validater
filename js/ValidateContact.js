function ValidateContact(contact,i)
{
    var regexp = /^\d{10}$/;
    if(!(contact.match(regexp)))
    {
        if(contact == "")
        {
            var textdata = document.createTextNode("Contact No is Mandatory in " + i + " Row.")
            PrintData(textdata);
        }
        else
        {
            var textdata = document.createTextNode("Contact No should contain only 10 digits Numeric Value in " + i + " Row.")
            PrintData(textdata);
        }
    }
}
