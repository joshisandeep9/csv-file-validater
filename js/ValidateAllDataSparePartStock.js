function ValidateAllDataSparePartUpload(cells,i)
{
    
    var quantity = cells[0], sparePartId = cells[1], refurbished = cells[2], pointId = cells[3],
        type = cells[4], point = cells[5];
    
           
    ValidateQuantity(quantity,i);
    ValidateSparePartId(sparePartId,i);
    ValidateRefurbished(refurbished,i);
    ValidatePointId(pointId,i);
    ValidateType(type,i);
    ValidatePoint(point,i);

}