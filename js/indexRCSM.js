function Upload() 
{   
    var AllUserNameArray = [];
    var AllUserContactNo = [];
    var reader = new FileReader();
    var fileUpload = document.getElementById("fileUpload");
    reader.onload = function (e){
        data = Papa.parse(e.target.result).data;
        for (var i = 1; i < data.length; i++) 
        {   
            var cells = data[i];  
            AllUserNameArray.push(cells[1]);  
            AllUserContactNo.push(cells[9]);               
            ValidateAllData(cells,i);                                  
        }
        CheckDublicateUserName(AllUserNameArray);      
        CheckDublicateContactNo(AllUserContactNo);           
    }
    reader.readAsText(fileUpload.files[0]);
}


