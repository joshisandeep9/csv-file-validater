function ValidateAllDataServiceCenter(cells,i)
{
    var id = cells[0], SystemId = cells[1], CreatedBy = cells[2], CreationDate = cells[3],
        CreationTime = cells[4], LastModifiedBy = cells[5], LastModificationDate = cells[6],
        LastModificationTime = cells[7], name = cells[8], type = cells[9], customerCode = cells[10],
        contact = cells[11], secondaryContact = cells[12], email = cells[13], imageId = cells[14],
        pincode = cells[15], region = cells[16], city = cells[17], address = cells[18], state = cells[19], 
        stateCode = cells[20], status = cells[21], gstin = cells[22], serviceTypes = cells[23],
        latitude = cells[24], longitude = cells[25];
    
    ValidateID(id,i);
    ValidateSystemId(SystemId,i);
    ValidateCreatedBy(CreatedBy,i);
    ValidateCreationDate(CreationDate,i);
    ValidateCreationTime(CreationTime,i);
    ValidateLastModifiedBy(LastModifiedBy,i);
    ValidateLastModificationDate(LastModificationDate,i);
    ValidateLastModificationTime(LastModificationTime,i);
    ValidateName(name,i);
    ValidateServiceCenterType(type,i);
    ValidateCustomerCode(customerCode,i);
    ValidateContact(contact,i);
    ValidateSecondaryContact(secondaryContact,i);
    ValidateEmail(email,i);
    ValidateImageId(imageId,i);
    ValidatePinCode(pincode,i);
    ValidateRegion(region,i);
    ValidateCity(city,i);
    ValidateAddress(address,i);
    ValidateState(state,i);
    ValidateStateCode(stateCode,i);
    ValidateStatus(status,i);
    ValidateGST(gstin,i);
    ValidateServiceTypes(serviceTypes,i);
    ValidateLatitude(latitude,i);
    ValidateLongitude(longitude,i);   
      
}