function ValidateAllData(cells,i)
{
    
    var brand = cells[0], bu = cells[1], category = cells[2], sku = cells[3],skuDescription = cells[4], skuGroup = cells[5],
        replacementApprovalNeeded = cells[6], unitPrice = cells[7], customerPrice = cells[8], gst = cells[9],
        hsnCode = cells[10], mrp = cells[11], status = cells[12], tatHours = cells[13], warranty = cells[14],
        amcAvailable = cells[15], amcMaxTerms = cells[16], amcPrice = cells[17],amcTerms = cells[18], 
        localServiceCharges = cells[19], upcountryServiceCharges = cells[20];


    ValidateBrand(brand,i);
    ValidateBu(bu,i);
    Validatecategory(category,i);
    ValidateSku(sku,i);
    ValidateSkuDescription(skuDescription,i);
    ValidateSkuGroup(skuGroup,i);
    ValidateReplacementApprovalNeeded(replacementApprovalNeeded,i);
    ValidateUnitPrice(unitPrice,i);
    ValidateCustomerPrice(customerPrice,i);
    ValidateGST(gst,i);
    ValidateHsnCode(hsnCode,i);
    ValidateMRP(mrp,i);
    ValidateStatus(status,i);
    ValidateTatHours(tatHours,i);
    ValidateWarranty(warranty,i);
    ValidateAmcAvailable(amcAvailable,i);
    ValidateAmcMaxTerms(amcMaxTerms,i);
    ValidateAmcPrice(amcPrice,i);
    ValidateAmcTerms(amcTerms,i);
    ValidateLocalServiceCharges(localServiceCharges,i);
    ValidateUPcountryServiceCharges(upcountryServiceCharges,i);
}











       

