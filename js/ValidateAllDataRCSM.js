function ValidateAllData(cells,i)
{
    
    var serviceCentreId = cells[0], username = cells[1], status = cells[2], qualification = cells[3], language = cells[4],
        experience = cells[5], firstName = cells[6], middleName = cells[7], lastName = cells[8], contact = cells[9],
        email = cells[10], gender = cells[11], imageId = cells[12], address = cells[13], flatno = cells[14],
        landmark = cells[15], city = cells[16], locality = cells[17], district = cells[18], state = cells[19], 
        statecode = cells[20], country = cells[21], pincode = cells[22];
        
    
    ValidateserviceCentreId(serviceCentreId,i);
    ValidateUserName(username,i)
    ValidateStatus(status,i);
    ValidateQualification(qualification,i);
    ValidateLanguage(language,i);
    ValidateExperience(experience,i);
    ValidateFirstName(firstName,i);
    ValidateMiddleName(middleName,i);
    ValidateLastName(lastName,i);
    ValidateContact(contact,i);
    ValidateEmail(email,i);
    ValidateGender(gender,i);
    ValidateImageId(imageId,i);
    ValidateAddress(address,i);
    ValidateFlatno(flatno,i);
    ValidateLandmark(landmark,i);
    ValidateCity(city,i);
    ValidateLocality(locality,i);
    ValidateDistrict(district,i);
    ValidateState(state,i);
    ValidateStateCode(statecode);
    ValidateCountry(country,i);
    ValidatePinCode(pincode,i)
}



        

