function ValidateFirstName(firstName,i)
{
    var letters = /^[A-Za-z]+$/;
    if(!(firstName.match(letters)))
    {
        if(firstName == "")
        {
            var textdata = document.createTextNode("First name is Mandatory in " + i + " Row.")
            PrintData(textdata);
        }
        else
        {
            var textdata = document.createTextNode("First name should contain only Alphabets in " + i + " Row.")
            PrintData(textdata);
        }
    } 
}
